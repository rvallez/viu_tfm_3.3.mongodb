/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mongotest2;


import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import java.net.UnknownHostException;

/**
 *
 * @author rvallez
 */
public class MongoDBTweetExample {
    public static void main(String[] args) throws UnknownHostException {
	
		MongoClient mongo = new MongoClient("localhost", 27017);
		DB db = mongo.getDB("test");
		
		DBCollection col = db.getCollection("tweets");
		
                //read example
		DBObject query = BasicDBObjectBuilder.start().add("user.name", "Barack Obama").get();
		DBCursor cursor = col.find(query);
		while(cursor.hasNext()){
			System.out.println(cursor.next());
		}
                mongo.close();
    }
    

    
}
